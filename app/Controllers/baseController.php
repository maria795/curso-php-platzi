<?php
namespace App\Controllers;

class baseController 
{
	protected $templateEngine;

	public function __construct()
	{
		$loader = new \Twig_Loader_Filesystem('../view');

		$this->templateEngine = new \Twig_Environment($loader, array(
		    'cache' => false,
		    'debug' => true,
		));		
	} 
	public function renderHTML($fileName, $data = [])
	{
		return $this->templateEngine->render($fileName, $data);
	}
}