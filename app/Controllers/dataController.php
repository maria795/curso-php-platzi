<?php
namespace App\Controllers;
use App\Models\Data;

class dataController extends baseController {
  
   function index() {
		$data = Data::all();
		return $data;
	}

	function addPersonalData($request)
	{
		if($request->getMethod() == "POST") 
		{
			$postData = $request->getParsedBody();
			$data = new Data();

			$data->name = $postData['name'];
			$data->profession = $postData['profession'];
			$data->email  = $postData['email'];
			$data->profession  = $postData['profession'];
			$data->career_summary  = $postData['career_summary'];
			$data->save();
			echo $this->renderHTML('addData.twig');
		}
		else if($request->getMethod() == "GET") 
		{
			echo $this->renderHTML('addData.twig');
		}
	}

}

?>