<?php
namespace App\Controllers;
use App\Models\Project;

class projectController extends baseController{

	function getAddProjectAction($request)
	{
		if($request->getMethod() == "POST") 
		{
			$postData = $request->getParsedBody();
			$data = new Project();
			$data->name = $postData['name'];
			$data->description = $postData['description'];
			$data->description = $postData['duration'];
			$data->technology  = $postData['technology'];
			$data->save();
			echo $this->renderHTML('addProject.twig');
		}
		else if($request->getMethod() == "GET") 
		{
			echo $this->renderHTML('addProject.twig');
		}
	}
	
}

?>