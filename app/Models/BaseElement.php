<?php
namespace App\Models;

class BaseElement {

	public $title;
	public $description;
	public $visible;
	public $months;

  function getTitle($title)
  {
      return $title;
  }
  function getDescription($description)
  {
    return $description;
  }
  function getVisible($visible)
  {
    return $visible;
  }
  function getMonth($months)
  {
    return $months;
  }
  function getDuration()
  {
    $years = floor($this->months / 12);
    $years_residuo = $this->months % 12;

    if ($years == 0 and $years_residuo != 0) {
      echo $years_residuo ." "."meses";
    }
    else if ($years != 0 and $years_residuo == 0)  
    {
      echo $years ." "."años";
    }
    elseif ($years != 0 and $years_residuo != 0) 
    {
      echo $years ." "."años y"." " .$years_residuo ." " ."meses";
    }
    else
    {
      echo "Sin fecha";
    }
  }
}