<?php
namespace App\Models;
require_once "../vendor/autoload.php";

use Illuminate\Database\Eloquent\Model;

class Job extends Model {

  protected $table = "jobs";

  function getDuration($months)
  {
    $years = floor($months / 12);
    $years_residuo = $months % 12;

    if ($years == 0 and $years_residuo != 0) {
      echo $years_residuo ." "."meses";
    }
    else if ($years != 0 and $years_residuo == 0)  
    {
      echo $years ." "."años";
    }
    elseif ($years != 0 and $years_residuo != 0) 
    {
      echo $years ." "."años y"." " .$years_residuo ." " ."meses";
    }
    else
    {
      echo "Sin fecha";
    }
  }

}
