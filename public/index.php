<?php
ini_set('display_errors', 1);
ini_set('display_starup_error', 1);
error_reporting(E_ALL);

require_once "../vendor/autoload.php";

use Illuminate\Database\Capsule\Manager as Capsule;
use App\Models\Job;
use Aura\Router\RouterContainer;

$capsule = new Capsule;
$capsule->addConnection([
    'driver'    => 'mysql',
    'host'      => 'localhost',
    'database'  => 'curso_php',
    'username'  => 'root',
    'password'  => '1234',
    'charset'   => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix'    => '',
]);

$capsule->setAsGlobal();
$capsule->bootEloquent();

$request = Zend\Diactoros\ServerRequestFactory::fromGlobals(
    $_SERVER,
    $_GET,
    $_POST,
    $_COOKIE,
    $_FILES
);

$routerContainer = new RouterContainer();
$map = $routerContainer->getMap();

$map->get('index', '/curso/index', [
    'controller'=>"App\Controllers\indexController",
    'action'=> "indexAction"
]);

$map->get('addJob', '/curso/job/add',[
    'controller'=>"App\Controllers\jobController",
    'action'=> "getAddJobAction"
]);
$map->post('saveJob', '/curso/job/add',[
    'controller'=>"App\Controllers\jobController",
    'action'=> "getAddJobAction"
]);

$map->get('addProject', '/curso/project/add',[
    'controller'=>"App\Controllers\projectController",
    'action'=> "getAddProjectAction"
]);

$map->post('saveProject', '/curso/project/add',[
    'controller'=>"App\Controllers\projectController",
    'action'=> "getAddProjectAction"
]);

$map->get('addData', '/curso/data/add',[
    'controller'=>"App\Controllers\dataController",
    'action'=> "addPersonalData"
]);

$map->post('saveData', '/curso/data/add',[
    'controller'=>"App\Controllers\dataController",
    'action'=> "addPersonalData"
]);

$matcher = $routerContainer->getMatcher();
$route = $matcher->match($request);

if (!$route) {
    echo "No route";
}
else {
    $handlerData = $route->handler; 
    $controllerName = $handlerData['controller'];
    $actionName = $handlerData['action']; 

    $controller = new $controllerName;
    $controller->$actionName($request);
}

?>